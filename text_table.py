import pandas as pd
import pyspark as ps

def read_raw():
    path = "/home/artur/Documents/3_course/Spec/secprocessorstud/Processor/MouseDyn/Data/training_files/user16/session_0735651357"
    df = pd.read_csv(path, delimiter=',')
    df.info()
    with open("session_test.csv", "w") as f:
        df.to_csv(path_or_buf=f, sep='|', header=True)

def read_features():
    path = "/home/artur/Documents/3_course/Spec/secprocessorstud/.testing/orig.csv"
    df = pd.read_csv(path, delimiter=',')
    print(df.columns)
    df = df.drop(columns=['Unnamed: 0'])
    df.info()
    with open("session_feat.csv", "w") as f:
        df.to_csv(path_or_buf=f, sep='|', header=True)

if __name__ == "__main__":
    read_features()
