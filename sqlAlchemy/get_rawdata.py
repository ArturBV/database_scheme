from sqlalchemy import create_engine, select, MetaData, Table, and_
from datetime import datetime

ENGINE_NAME = 'sqlite:///csv_test_2.db'

# def get_rawdata(model_type : str, stage : int, user_id)

if __name__ == "__main__":
    engine = create_engine('sqlite:///csv_test_2.db')
    metadata = MetaData()
    table_name = "MD_session_rawdata"
    table = Table(
        table_name, 
        metadata, 
        autoload_with=engine
    )
    
    date_start = "2024-05-10T14:00:00.000"
    date_end = "2024-05-10T14:01:00.000"
    session_id = 1
    username = "shetinin"
    #time = datetime.strptime(time,"%Y-%m-%dT%H:%M:%S.%f")
    stmt = select(
        table.columns.time,
        table.columns.x_pos,
        table.columns.y_pos,
    ).where(and_(
        table.columns.username == username,
        table.columns.intsession_id == session_id,
        table.columns.time > datetime.strptime(date_start, "%Y-%m-%dT%H:%M:%S.%f"),
        table.columns.time < datetime.strptime(date_end, "%Y-%m-%dT%H:%M:%S.%f")
    ))

    connection = engine.connect()
    results = connection.execute(stmt).fetchall()

    print(*results, sep='\n')
