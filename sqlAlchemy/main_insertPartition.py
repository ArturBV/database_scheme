from new_tables import User, Session, SessionRawdata, SessionFeatures
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from functools import partial

from utils import get_user_id_or_create, get_session_id_or_create

from test_spark import get_test_data

from config_db import get_engine_db

USER_ID_MAP = {
    "bashirov": 188
}

# engine = create_engine('postgresql://postgres:000000@localhost:5432/new_beg')
def insert_partition_to_db(partition, username, intsession_id, SessionLocal):
    # Create a new SQLAlchemy session per partition
    session = SessionLocal()
    username_parsed, user_id_parsed = parse_username_userid(username)
    user_id_ = get_user_id_or_create(session, user_id_parsed, username_parsed)
    session_id_ = get_session_id_or_create(session, user_id_, intsession_id)

    # Initialize a list to accumulate new entries for batch insert
    entries = []

    # Batch size (you can tweak this)
    batch_size = 100

    # Iterate over the rows in the partition
    try:
        for row in partition:
            row_data = row.asDict()

            new_entry = SessionRawdata(
                type=0,
                user_id=user_id_,
                username=username,
                s_id=session_id_,
                data=row_data
            )
            
            # Append to the batch
            entries.append(new_entry)

            # Once the batch is full, insert into the database
            if len(entries) >= batch_size:
                session.bulk_save_objects(entries)
                session.commit()  # Commit after each batch
                entries = []  # Reset batch list

        # Insert any remaining entries after the loop
        if entries:
            session.bulk_save_objects(entries)
            session.commit()
    except Exception as e:
        session.rollback()
        print("Oopsie! Something went wrong!")
        raise e
    # Close the session
    session.close()


if __name__ == "__main__":
    engine = get_engine_db("new_beg")
    SessionLocal = sessionmaker(bind=engine)

    jdbc_url = 'jdbc:postgresql://localhost:5432/new_beg'
    properties = {
        'user': 'postgres',
        'password': '000000',
        'driver': 'org.postgresql.Driver'
    }

    username, intsession_id, df = get_test_data()  # Get

    # Use foreachPartition to process partitions
    insert_partition_to_db_session = partial(insert_partition_to_db,
                                             username=username,
                                             intsession_id=intsession_id,
                                             SessionLocal=SessionLocal)
    df.foreachPartition(insert_partition_to_db_session)
    

    
