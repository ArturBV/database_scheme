from sqlalchemy import create_engine, select, MetaData, Table, and_
from datetime import datetime
from config_db import get_engine_db



if __name__ == "__main__":
    engine = get_engine_db()
    metadata = MetaData()
    table_name = "MD_session_features"
    table = Table(
        table_name, 
        metadata, 
        autoload_with=engine
    )
    
    # date_start = "2024-05-10T14:00:00.000"
    # date_end = "2024-05-10T14:01:00.000"
    session_id = 11
    # username = "shetini"
    user_id = 188
    limit_value = 1000
    #time = datetime.strptime(time,"%Y-%m-%dT%H:%M:%S.%f")
    stmt = select(
        table
    ).where(and_(
        table.columns.user_id == user_id,
        table.columns.s_session_id == session_id,
    )).limit(limit_value)


    connection = engine.connect()
    results = connection.execute(stmt).fetchall()

    print(*results, sep='\n')
