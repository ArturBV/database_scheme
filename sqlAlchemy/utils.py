from new_tables import User, Session

def create_user(sql_session, user_id, username):
    new_user = User(user_id=user_id, username=username)
    sql_session.add(new_user)
    sql_session.commit()
    return new_user.user_id

def create_session(sql_session, user_id, session_id):
    new_session = Session(s_id=session_id, user_id=user_id, params={})
    sql_session.add(new_session)
    sql_session.commit()
    return new_session.s_id

def get_user_id_or_create(sql_session, user_id_, username):
    user_obj = sql_session.query(User).get(user_id_)
    if user_obj is None:
        print(f"Creating USER [{user_id_}, {username}, -]")
        user_id = create_user(sql_session, user_id_, username)
    else:
        user_id = user_obj.user_id
    return user_id

def get_session_id_or_create(sql_session, user_id, intsession_id):
    session_obj = sql_session.query(Session).get(intsession_id)
    if session_obj is None:
        print(f"Creating SESSION [{user_id}, {intsession_id}, -, -]")
        session_id = create_session(sql_session, user_id, intsession_id)
    else:
        session_id = session_obj.s_id
    return session_id


def get_user_sessions(username):
    # Create a new session
    db = SessionLocal()

    try:
        # Query the user by username
        user = db.query(User).filter(User.username == username).first()

        if user:
            # Access all sessions associated with the user
            user_sessions = user.sessions
            return user_sessions
        else:
            print("User not found")
            return None
    finally:
        # Close the session
        db.close()

def parse_username_userid(username):
    if "@" not in username:
        raise ValueError(f"username must be in format \'<username>@<user_id>,\'\n\t\tgiven username: {username}")
    return username[:username.find("@")], int(username[username.find("@")+1:])