# add_features.py
from pprint import pprint
from sqlalchemy import create_engine, FLOAT, INTEGER, BIGINT
from sqlalchemy.orm import sessionmaker
from tables import Base, MD_session_features
from sqlalchemy.orm import sessionmaker
from tqdm import tqdm
from sqlalchemy import Column, Integer, String, ForeignKey, LargeBinary
from tables import Session, SessionParam, User, MD_session_rawdata, KD_session_rawdata
from datetime import datetime
from numpy import genfromtxt
import pandas as pd

# Database connection setup
# DATABASE_URL = "postgresql://username:password@localhost/dbname"
# engine = create_engine(DATABASE_URL)
# Session = sessionmaker(bind=engine)
# session = Session()
def Load_Data(file_name, skip_header=True, delimiter=','):
    for line in open(file_name, "r"):
        if skip_header:
            skip_header=False
            continue
        line_query = line.strip().split(delimiter)
        if len(line_query) == 1 and line_query[0] == '': # skip empty row
            continue
        #print(line_query)
        yield [line_query]
def create_user(sql_session, user_id, user_name, some_info):
    user = User(user_id=user_id, user_name=user_name, some_info=some_info)
    sql_session.add(user)
    sql_session.commit()
    return user.user_id

# Function to create a new session
def create_session(sql_session, user_id, session_id, os_family, systeminfo):
    user = sql_session.query(User).get(user_id)
    if user:
        new_session = Session(id=session_id, user=user)
        session_param = SessionParam(id=session_id, os_family=os_family, systeminfo=systeminfo.encode())
        #new_session.session_params = session_param.session
        sql_session.add(new_session)
        sql_session.commit()
        sql_session.add(session_param)
        sql_session.commit()
        return new_session.id
    else:
        return None

USERS = {"shetinin": 1}

def get_user_id_or_create(sql_session, user_id_, username=None):
    user_obj = sql_session.query(User).get(user_id_)
    if user_obj is None:
        print(f"Creating USER [{user_id_}, {username}, -]")
        user_id = create_user(sql_session, user_id_, username, "-")
    else:
        user_id = user_obj.user_id
    return user_id

def get_session_id_or_create(sql_session, user_id, intsession_id):
    session_obj = sql_session.query(Session).get(intsession_id)
    if session_obj is None:
        print(f"Creating SESSION [{user_id}, {intsession_id}, -, -]")
        session_id = create_session(sql_session, user_id, intsession_id, 0, "-")
    else:
        session_id = session_obj.id
    return session_id

TYPES_MAP = {"FLOAT": float,
             "INTEGER": int,
             "BIGINT": int,}

def parse_username_userid(username):
    if "@" not in username:
        raise ValueError(f"username must be in format \'<username>@<user_id>,\'\n\t\tgiven username: {username}")
    return username[:username.find("@")], int(username[username.find("@")+1:])
md_feat_names = [
       #'user_id', 'session_id', 
       'start_time', 'end_time', 'raw_ids',
       'direction_bin', 'actual_distance', 'actual_distance_bin',
       'curve_length', 'curve_length_bin', 'length_ratio', 'actual_speed',
       'curve_speed', 'curve_acceleration', 'mean_movement_offset',
       'mean_movement_error', 'mean_movement_variability', 'mean_curvature',
       'mean_curvature_change_rate', 'mean_curvature_velocity',
       'mean_curvature_velocity_change_rate', 'mean_angular_velocity', 'min_x',
       'max_x', 'mean_x', 'std_x', 'max_min_x', 'min_y', 'max_y', 'mean_y',
       'std_y', 'max_min_y', 'min_vx', 'max_vx', 'mean_vx', 'std_vx',
       'max_min_vx', 'min_vy', 'max_vy', 'mean_vy', 'std_vy', 'max_min_vy',
       'min_v', 'max_v', 'mean_v', 'std_v', 'max_min_v', 'min_a', 'max_a',
       'mean_a', 'std_a', 'max_min_a', 'min_j', 'max_j', 'mean_j', 'std_j',
       'max_min_j', 'min_am', 'max_am', 'mean_am', 'std_am', 'max_min_am',
       'min_av', 'max_av', 'mean_av', 'std_av', 'max_min_av', 'min_c', 'max_c',
       'mean_c', 'std_c', 'max_min_c', 'min_ccr', 'max_ccr', 'mean_ccr',
       'std_ccr', 'max_min_ccr', 'duration_of_movement', 'straightness', 'TCM',
       'SC', 'M3', 'M4', 'TCrv', 'VCrv']
# def load_md_session_features(md_session_rawdata_row_id, direction_bin, actual_distance, actual_distance_bin, curve_length, curve_length_bin, length_ration, actual_speed, curve_speed, curve_acceleration, mean_movement_offset, mean_movement_error, mean_movement_variability, mean_curvature, mean_curvature_change_rate, mean_curvature_velocity, mean_curvature_velocity_change_rate, mean_angular_velocity, min_x, max_x, mean_x, std_x, max_min_x, min_y, max_y, mean_y, std_y, max_mix_y, min_vx, max_vx, mean_vx, std_vx, max_mix_vx, min_vy, max_vy, mean_vy, std_vy, max_mix_vy, min_v, max_v, mean_v, std_v, max_mix_v, min_a, max_a, mean_a, std_a, max_mix_a, min_j, max_j, mean_j, std_j, max_mix_j, min_am, max_am, mean_am, std_am, max_mix_am, min_av, max_av, mean_av, std_av, max_mix_av, min_c, max_c, mean_c, std_c, max_mix_c, min_ccr, max_ccr, mean_ccr, std_ccr, max_min_ccr, duration_of_movement, straightness, TCM, SC, M3, M4, TCrv, VCrv):
def load_md_session_features(sql_session, user_id_from_csv, intsession_id, *args, **kwargs):

    user_id = get_user_id_or_create(sql_session, user_id_from_csv)
    session_id = get_session_id_or_create(sql_session, user_id, intsession_id)

    if user_id and session_id:
#         if SQLITE:
# #d1 = datetime.datetime.strptime("2013-07-12T07:00:00Z","%Y-%m-%dT%H:%M:%SZ")
# #"2024-05-10T14:00:50.743"
#             time = datetime.strptime(time,"%Y-%m-%dT%H:%M:%S.%f")
        map_kw = dict(zip(md_feat_names, args))
        # pprint(args)
        # pprint(map_kw)
        for column in MD_session_features.__table__.columns:
            # if column.name in map_kw.keys():
                # print(column.name, str(column.type), column.type in TYPES_MAP.keys(), map_kw[column.name])
            col_type = str(column.type)
            if col_type in TYPES_MAP.keys() and column.name in map_kw.keys():
                map_kw[column.name] = TYPES_MAP[col_type](float(map_kw[column.name]))
        # pprint(map_kw)
        md_session_feature = MD_session_features(
                row_id=None, # row_id
                MD_session_rawdata_row_id=1, #rawdata_row_id_reference
                s_session_id=session_id, # session_id
                user_id=user_id, # user_id
                **map_kw
                # *args[2:]
            )
        # print(f"\n\nCOLUMNS OF TABLE: {md_session_feature.__table__.columns.n}\n\n{MD_session_features.__table__.columns}\n\n")
        sql_session.add(md_session_feature)
        sql_session.commit()
        return md_session_feature.row_id
    else:
        return None

    """
    md_session_feature = MD_session_features(
        MD_session_rawdata_row_id=md_session_rawdata_row_id,
        direction_bin=direction_bin,
        actual_distance=actual_distance,
        actual_distance_bin=actual_distance_bin,
        curve_length=curve_length,
        curve_length_bin=curve_length_bin,
        length_ration=length_ration,
        actual_speed=actual_speed,
        curve_speed=curve_speed,
        curve_acceleration=curve_acceleration,
        mean_movement_offset=mean_movement_offset,
        mean_movement_error=mean_movement_error,
        mean_movement_variability=mean_movement_variability,
        mean_curvature=mean_curvature,
        mean_curvature_change_rate=mean_curvature_change_rate,
        mean_curvature_velocity=mean_curvature_velocity,
        mean_curvature_velocity_change_rate=mean_curvature_velocity_change_rate,
        mean_angular_velocity=mean_angular_velocity,
        min_x=min_x,
        max_x=max_x,
        mean_x=mean_x,
        std_x=std_x,
        max_min_x=max_min_x,
        min_y=min_y,
        max_y=max_y,
        mean_y=mean_y,
        std_y=std_y,
        max_mix_y=max_mix_y,
        min_vx=min_vx,
        max_vx=max_vx,
        mean_vx=mean_vx,
        std_vx=std_vx,
        max_mix_vx=max_mix_vx,
        min_vy=min_vy,
        max_vy=max_vy,
        mean_vy=mean_vy,
        std_vy=std_vy,
        max_mix_vy=max_mix_vy,
        min_v=min_v,
        max_v=max_v,
        mean_v=mean_v,
        std_v=std_v,
        max_mix_v=max_mix_v,
        min_a=min_a,
        max_a=max_a,
        mean_a=mean_a,
        std_a=std_a,
        max_mix_a=max_mix_a,
        min_j=min_j,
        max_j=max_j,
        mean_j=mean_j,
        std_j=std_j,
        max_mix_j=max_mix_j,
        min_am=min_am,
        max_am=max_am,
        mean_am=mean_am,
        std_am=std_am,
        max_mix_am=max_mix_am,
        min_av=min_av,
        max_av=max_av,
        mean_av=mean_av,
        std_av=std_av,
        max_mix_av=max_mix_av,
        min_c=min_c,
        max_c=max_c,
        mean_c=mean_c,
        std_c=std_c,
        max_mix_c=max_mix_c,
        min_ccr=min_ccr,
        max_ccr=max_ccr,
        mean_ccr=mean_ccr,
        std_ccr=std_ccr,
        max_min_ccr=max_min_ccr,
        duration_of_movement=duration_of_movement,
        straightness=straightness,
        TCM=TCM,
        SC=SC,
        M3=M3,
        M4=M4,
        TCrv=TCrv,
        VCrv=VCrv
    )
    """
    # Add the instance to the session and commit
    session.add(md_session_feature)
    session.commit()

from sqlalchemy.orm import sessionmaker
from tqdm import tqdm
import pandas as pd

def load_md_session_features_from_csv(engine, csv_path):
    # Create a session
    sqlSession_cl = sessionmaker(bind=engine)
    session = sqlSession_cl()
    
    # Load data from CSV
    data_gen = Load_Data(csv_path, delimiter='\t')
    
    try:
        for data_batch in tqdm(data_gen):
            for row in data_batch:
                # print("\n\n!!!ROW LEN: ", len(row), "\n", row, "\n")
                load_md_session_features(session, *row)
    except Exception as e:
        session.rollback()
        raise e
    finally:
        session.close()

# Example usage
if __name__ == "__main__":
    print("Hello, World!")