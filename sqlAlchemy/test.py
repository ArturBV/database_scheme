from tables import MD_session_rawdata, Base
from add_rawdata import *
from add_features import *

from config_db import USERNAME, PASSWORD, HOSTNAME, PORT, DATABASE_NAME

from numpy import genfromtxt
from time import time
from datetime import datetime
from sqlalchemy import Column, Integer, Float, Date
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker



if __name__ == "__main__":
    t = time()
    
    # username = "postgres"
    # # password = "Qwer17345"
    # password = "000000"
    # hostname = "localhost"
    # port = "5432"
    # database_name = "postgres"
    engine = create_engine(f'postgresql://{USERNAME}:{PASSWORD}@{HOSTNAME}:{PORT}/{DATABASE_NAME}')
    
   #engine = create_engine(f'postgresql://{username}:{password}/{database_name}')

    #Create the database
    # engine = create_engine('sqlite:///csv_test_3.db')
    Base.metadata.create_all(engine)

    #Create the session
    #session = sessionmaker()
    #session.configure(bind=engine)
    #s = session()
    try:
        # file_name = "MOUSE_art.csv" 
        file_name = "MOUSE_feat_exp.tsv" 
        #file_name = "KEYBOARD.csv" 
        # add_session_rawdata_from_csv(engine=engine, type="md", csv_path=file_name)
        load_md_session_features_from_csv(engine=engine, csv_path=file_name)
    #except Exception as e:
    #    print("SMTH WRONG")
    #    print(e)
    finally:
        print("Time elapsed: " + str(time() - t) + " s.") #0.091s

