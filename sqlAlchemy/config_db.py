from sqlalchemy import create_engine


USERNAME = "postgres"
PASSWORD = "Qwer17345" #magus
#PASSWORD = "000000" #local
HOSTNAME = "localhost"
PORT = "5432"
DATABASE_NAME = "postgres"

def get_engine_db(dbname:str = None):
    if dbname is None:
        dbname = DATABASE_NAME
    return create_engine(f'postgresql://{USERNAME}:{PASSWORD}@{HOSTNAME}:{PORT}/{dbname}')

