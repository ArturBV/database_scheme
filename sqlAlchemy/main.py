from pprint import pprint
from new_tables import User, Session, SessionRawdata, SessionFeatures, ALL_TABLES_CLS
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from functools import partial
import json
from tqdm import tqdm
import logging

from utils import get_user_id_or_create, get_session_id_or_create, parse_username_userid

from test_spark import get_test_data, get_test_features, spark
import pyspark.sql.functions as f
import pyspark as ps

from config_db import get_engine_db

USER_ID_MAP = {
    "bashirov": 188
}

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


# engine = create_engine('postgresql://postgres:000000@localhost:5432/new_beg')
def insert_df_to_db(df, table_name, username, intsession_id, SessionLocal):
    # Create a new SQLAlchemy session per partition
    session = SessionLocal()
    logger.info(f"Inserting data for {username} with session_id {intsession_id}")


    username_parsed, user_id_parsed = parse_username_userid(username)
    user_id_ = get_user_id_or_create(session, user_id_parsed, username_parsed)
    session_id_ = get_session_id_or_create(session, user_id_, intsession_id)


    table_class = None
    for cls in ALL_TABLES_CLS:
        if cls.__name__ == table_name:
            table_class = cls
            break
    if table_class is None:
        raise ValueError(f"Table {table_name} not found in the database")
    # Iterate over the rows in the partition
    try:
        # Iterate over the rows in the partition
        for row in tqdm(df.toJSON().collect()):
            # Convert the PySpark Row object to a dictionary
            row_data = json.loads(row)

            # Example: Insert the JSON data into the SQLAlchemy model
            new_entry = table_class(
                type=0,
                user_id=user_id_,
                s_id=session_id_,
                data=row_data
            )
            
            # Add the entry to the session
            session.add(new_entry)

        # Commit the transaction
        session.commit()
    except Exception as e:
        session.rollback()
        print("Oopsie! Something went wrong!")
        raise e
    # Close the session
    session.close()

def get_df_from_db(spark, table_name, username, intsession_id, SessionLocal):
    # Create a new SQLAlchemy session per partition
    session = SessionLocal()
    logger.info(f"Getting data for {username} with session_id {intsession_id}")

    username_parsed, user_id_parsed = parse_username_userid(username)
    user_id_ = get_user_id_or_create(session, user_id_parsed, username_parsed)
    session_id_ = get_session_id_or_create(session, user_id_, intsession_id)

    table_class = None
    for cls in ALL_TABLES_CLS:
        if cls.__name__ == table_name:
            table_class = cls
            break
    if table_class is None:
        raise ValueError(f"Table {table_name} not found in the database")
    # Iterate over the rows in the partition
    try:
        # Iterate over the rows in the partition
        data = (
            session
            .query(table_class.data)
            .filter_by(user_id=user_id_, s_id=session_id_)
            .all()
        )
        data = [d[0] for d in data]
        # pprint(data[:10])
        df_data = spark.createDataFrame(data)
        df_data_columns = df_data.columns
        # df_data = df_data.withColumn("user_id", f.lit(user_id_).cast("string"))
        # df_data = df_data.withColumn("intsession_id", f.lit(session_id_).cast("string"))
        # return df_data.select("user_id", "intsession_id", *df_data_columns)
        return df_data

    except Exception as e:
        session.rollback()
        print("Oopsie! Something went wrong!")
        raise e
    # Close the session
    session.close()


if __name__ == "__main__":
    engine = get_engine_db(dbname="new_beg")
    SessionLocal = sessionmaker(bind=engine)

    # TESTS = {3, 4}
    TESTS = {1, 2}
   
    if 1 in TESTS:    
        username, intsession_id, df_ = get_test_data()  # Get
        logger.info(f"Inserting rawdata for {username} with session_id {intsession_id}")
        insert_df_to_db(df=df_,
                        table_name="SessionRawdata",
                        username=username,
                        intsession_id=intsession_id,
                        SessionLocal=SessionLocal)

    if 2 in TESTS:
        logger.info(f"Inserting features for {username} with session_id {intsession_id}")
        username, intsession_id, df_feat = get_test_features()  # Get
        insert_df_to_db(df=df_feat,
                        table_name="SessionFeatures",
                        username=username,
                        intsession_id=intsession_id,
                        SessionLocal=SessionLocal)
    
    if 3 in TESTS:
        username = "bashirov@188"
        intsession_id = 11
        logger.info(f"Getting rawdata for {username} with session_id {intsession_id}")
        df_rawdata = get_df_from_db(spark, table_name="SessionRawdata", username=username, intsession_id=intsession_id, SessionLocal=SessionLocal)
        pprint(df_rawdata.collect()[:10])

    if 4 in TESTS:
        username = "bashirov@188"
        intsession_id = 11
        logger.info(f"Getting features for {username} with session_id {intsession_id}")
        df_features = get_df_from_db(spark, table_name="SessionFeatures", username=username, intsession_id=intsession_id, SessionLocal=SessionLocal)
        pprint(df_features.collect()[:10])


    

    
