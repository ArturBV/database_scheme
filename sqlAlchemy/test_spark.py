import pyspark as ps
from typing import Tuple

spark = (
    ps.sql.SparkSession
    .builder
    .appName("test")
    .config("spark.driver.memory", "2g") 
    .config("spark.executor.memory", "2g") 
    .getOrCreate()
)

def get_test_data() -> Tuple[int, int, ps.sql.DataFrame] :
    df = spark.read.csv("MOUSE_art.csv", header=True)
    
    info_cols = ["username", "intsession_id"]
    data_cols = [col for col in df.columns if col not in info_cols]

    username = df.select("username").distinct().collect()[0]["username"]
    intsession_id = df.select("intsession_id").distinct().collect()[0]["intsession_id"]
    used_data = df.select(*data_cols)
    return username, intsession_id, used_data

def get_test_features() -> Tuple[int, int, ps.sql.DataFrame] :
    df = spark.read.csv("MOUSE_feat_exp.csv", header=True)
    
    info_cols = ["username", "intsession_id"]
    data_cols = [col for col in df.columns if col not in info_cols]

    username = df.select("username").distinct().collect()[0]["username"]
    intsession_id = df.select("intsession_id").distinct().collect()[0]["intsession_id"]
    used_data = df.select(*data_cols)
    return username, intsession_id, used_data

if __name__ == "__main__":
    username, intsession_id, df_ = get_test_data()  # Get
    print(df_.head(10))
    username, intsession_id, df_feat = get_test_features()  # Get
    print(df_feat.head(10))