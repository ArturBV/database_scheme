from sqlalchemy.orm import sessionmaker
from tqdm import tqdm
from sqlalchemy import Column, Integer, String, ForeignKey, LargeBinary
from tables import Session, SessionParam, User, MD_session_rawdata, KD_session_rawdata
from datetime import datetime
from numpy import genfromtxt
import pandas as pd
# Create a session maker

READ_LINE_BUFFER = 50


# Function to create a new user
def create_user(sql_session, user_id, user_name, some_info):
    user = User(user_id=user_id, user_name=user_name, some_info=some_info)
    sql_session.add(user)
    sql_session.commit()
    return user.user_id

# Function to create a new session
def create_session(sql_session, user_id, session_id, os_family, systeminfo):
    user = sql_session.query(User).get(user_id)
    if user:
        new_session = Session(id=session_id, user=user)
        session_param = SessionParam(id=session_id, os_family=os_family, systeminfo=systeminfo.encode())
        #new_session.session_params = session_param.session
        sql_session.add(new_session)
        sql_session.commit()
        sql_session.add(session_param)
        sql_session.commit()
        return new_session.id
    else:
        return None

USERS = {"shetinin": 1}

def get_user_id_or_create(sql_session, user_id_, username):
    user_obj = sql_session.query(User).get(user_id_)
    if user_obj is None:
        print(f"Creating USER [{user_id_}, {username}, -]")
        user_id = create_user(sql_session, user_id_, username, "-")
    else:
        user_id = user_obj.user_id
    return user_id

def get_session_id_or_create(sql_session, user_id, intsession_id):
    session_obj = sql_session.query(Session).get(intsession_id)
    if session_obj is None:
        print(f"Creating SESSION [{user_id}, {intsession_id}, -, -]")
        session_id = create_session(sql_session, user_id, intsession_id, 0, "-")
    else:
        session_id = session_obj.id
    return session_id

def parse_username_userid(username):
    if "@" not in username:
        raise ValueError(f"username must be in format \'<username>@<user_id>,\'\n\t\tgiven username: {username}")
    return username[:username.find("@")], int(username[username.find("@")+1:])

SQLITE=True
# Function to add data to MD_session_rawdata
def add_md_session_rawdata(sql_session, time, username, winsession_id, intsession_id, hwnd_orig, processname, PID, hookmode, provocation, message_id, x_pos, y_pos, hwnd, hittest, extra_info):
    #def add_md_session_rawdata(user_id, sessin_id, record_timestamp, client_timestamp, button, state, x, y):
    
    username, user_id_from_csv = parse_username_userid(username)

    user_id = get_user_id_or_create(sql_session, user_id_from_csv, username)
    session_id = get_session_id_or_create(sql_session, user_id, intsession_id)

    if user_id and session_id:
        if SQLITE:
#d1 = datetime.datetime.strptime("2013-07-12T07:00:00Z","%Y-%m-%dT%H:%M:%SZ")
#"2024-05-10T14:00:50.743"
            time = datetime.strptime(time,"%Y-%m-%dT%H:%M:%S.%f")
        
        md_session_rawdata = MD_session_rawdata(
            #row_id=random.randint(0, 1000000000),
            row_id=None,
            User_user_id=user_id,
            Session_id=session_id,
            time=time,
            username=username,
            winsession_id=winsession_id.encode(),
			intsession_id=intsession_id,
			hwnd_orig=hwnd_orig.encode(),
			processname=processname,
			PID=PID,
			hookmode=hookmode,
			provocation=provocation,
			message_id=message_id,
			x_pos=x_pos,
			y_pos=y_pos,
			hwnd=hwnd.encode(),
			hittest=hittest,
			extra_info=extra_info,
        )
        sql_session.add(md_session_rawdata)
        sql_session.commit()
        return md_session_rawdata.row_id
    else:
        return None


def Load_Data_buf(file_name, skip_header=True, delimiter=','):
    num_of_cols = 0
    line_buffer = READ_LINE_BUFFER if READ_LINE_BUFFER > 1 else 1
    with open(file_name, "r") as f:
        if skip_header:
            header = f.readline().strip().split(delimiter)
            num_of_cols = len(header)
        for line_f in  f:
            buffer = list()
            for _ in range(READ_LINE_BUFFER):
                line = line_f.strip().split(delimiter)
                if len(line) == 1 and line[0] == '': # skip emppty row
                    continue
                buffer.append(line)
            yield buffer


def Load_Data(file_name, skip_header=True, delimiter=','):
    for line in open(file_name, "r"):
        if skip_header:
            skip_header=False
            continue
        line_query = line.strip().split(delimiter)
        if len(line_query) == 1 and line_query[0] == '': # skip empty row
            continue
        #print(line_query)
        yield [line_query]

def Load_Data_pd(file_name, skip_header=True, delimiter=','):
    data = pd.read_csv(file_name, header=0, delimiter=delimiter)
    return [data.values]

def add_md_session_rawdata_from_csv(engine, csv_path):
    #def add_md_session_rawdata(user_id, sessin_id, record_timestamp, client_timestamp, button, state, x, y):
    sqlSession_cl = sessionmaker(bind=engine)
    session = sqlSession_cl()
    data_gen = Load_Data(csv_path) 
    try:
        for data_batch in tqdm(data_gen):
            for row in data_batch:
                row_id = add_md_session_rawdata(session, *row)
            #print(f"added row with row_id: {row_id}")
    except Exception as e:
        session.rollback()
        raise e
    finally:
        session.close()

def add_session_rawdata_from_csv(engine:str, type:str, csv_path:str):
    TYPES = {"md": add_md_session_rawdata,
             "kd": add_kd_session_rawdata,
             "ms": None,
             "ks": None}
    try:
        add_func = TYPES[type.lower()]
    except Exception as e:
        raise Exception(f"Unavailable type! Use one of: md, kd")
    if add_func is None:
        raise Exception(f"Unavailable type! Use one of: md, kd") 
    
    sqlSession_cl = sessionmaker(bind=engine)
    session = sqlSession_cl()
    data_gen = Load_Data(csv_path) 
    try:
        for data_batch in tqdm(data_gen):
            for row in data_batch:
                row_id = add_func(session, *row)
    except Exception as e:
        session.rollback()
        raise e
    finally:
        session.close()


# Function to add data to KD_session_rawdata
def add_kd_session_rawdata(sql_session, time, username, winsession_id, intsession_id, hwnd_orig, processname, PID, hookmode, provocation, virtualcode, scancode, keyup, prev_keystate, repeat_count, alt_down, extended_kbd, langID, keybdID, raw_data_debug="0".encode()):
    
    username, user_id = parse_username_userid(username)
    user_id = get_user_id_or_create(sql_session, user_id, username)
    session_id = get_session_id_or_create(sql_session, user_id, intsession_id)
    if user_id and session_id:
        if SQLITE:
#d1 = datetime.datetime.strptime("2013-07-12T07:00:00Z","%Y-%m-%dT%H:%M:%SZ")
#"2024-05-10T14:00:50.743"
            time = datetime.strptime(time,"%Y-%m-%dT%H:%M:%S.%f")
        kd_session_rawdata = KD_session_rawdata(
            row_id=None,
            User_user_id=user_id,
            Session_id=session_id,
            time=time,
            username=username,
            winsession_id=winsession_id.encode(),
            intsession_id=session_id,
            hwnd_orig=hwnd_orig.encode(),
            processname=processname,
            PID=PID,
            hookmode=hookmode,
            provocation=provocation,
            virtualcode=virtualcode,
            scancode=scancode,
            keyup=bool(keyup),
            prev_keystate=bool(prev_keystate),
            repeat_count=repeat_count,
            alt_down=bool(alt_down),
            extended_kbd=extended_kbd,
            langID=langID.encode(),
            keybdID=keybdID.encode(),
            raw_data_debug=raw_data_debug
        )
        sql_session.add(kd_session_rawdata)
        sql_session.commit()
        return kd_session_rawdata.row_id
    else:
        return None

# Add similar functions for other *_rawdata entities
