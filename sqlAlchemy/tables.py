from sqlalchemy import Column, JSON, Integer, String, ForeignKey, LargeBinary, BigInteger, TIMESTAMP, Float, Boolean
from sqlalchemy import create_engine
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class User(Base):
    __tablename__ = 'User'

    user_id = Column(Integer, primary_key=True)
    user_name = Column(String)
    some_info = Column(String)

    sessions = relationship('Session', back_populates='user')
    kd_session_rawdata = relationship('KD_session_rawdata', back_populates='user')
    md_session_rawdata = relationship('MD_session_rawdata', back_populates='user')
    ms_session_rawdata = relationship('MS_session_rawdata', back_populates='user')
    ks_session_rawdata = relationship('KS_session_rawdata', back_populates='user')
    # md_session_features = relationship('MD_session_features', back_populates='user')

class Session(Base):
    __tablename__ = 'Session'

    id = Column(Integer, primary_key=True)

    User_user_id = Column(Integer, ForeignKey('User.user_id'))
    #KS_session_rawdata1row_id = Column(Integer)
    
    user = relationship('User', back_populates='sessions')
    session_params = relationship('SessionParam', back_populates='session')
    ks_session_rawdata = relationship('KS_session_rawdata', back_populates='session')
    ms_session_rawdata = relationship('MS_session_rawdata', back_populates='session')
    kd_session_rawdata = relationship('KD_session_rawdata', back_populates='session')
    md_session_rawdata = relationship('MD_session_rawdata', back_populates='session')
    # md_session_features = relationship('MD_session_features', back_populates='session')

class SessionParam(Base):
    __tablename__ = 'SessionParam'

    id = Column(Integer, primary_key=True)

    Session_id = Column(Integer, ForeignKey('Session.id'))
    
    os_family = Column(Integer)
    systeminfo = Column(LargeBinary)

    session = relationship('Session', back_populates='session_params')


#=============================================================================#
#====================MD=======================================================#
#=============================================================================#
class MD_session_rawdata(Base):
    __tablename__ = 'MD_session_rawdata'

    row_id = Column(BigInteger().with_variant(Integer, "sqlite"), primary_key=True)
    User_user_id = Column(Integer, ForeignKey('User.user_id'), nullable=False)
    Session_id = Column(Integer, ForeignKey('Session.id'), nullable=False)
    time = Column(TIMESTAMP)
    username = Column(String)
    winsession_id = Column(LargeBinary)
    intsession_id = Column(Integer)
    hwnd_orig = Column(LargeBinary)
    processname = Column(String)
    PID = Column(Integer)
    hookmode = Column(Integer)
    provocation = Column(Integer)
    message_id = Column(Integer)
    x_pos = Column(Integer)
    y_pos = Column(Integer)
    hwnd = Column(LargeBinary)
    hittest = Column(Integer)
    extra_info = Column(Integer)

    user = relationship('User', back_populates='md_session_rawdata')
    session = relationship('Session', back_populates='md_session_rawdata')
    buttons = relationship('Button', backref='md_session_rawdata')
    states = relationship('State', backref='md_session_rawdata')
    md_session_preprocessed = relationship('MD_session_preprocessed', backref='md_session_rawdata')
    md_session_features = relationship('MD_session_features', backref='md_session_rawdata')

class Button(Base):
    __tablename__ = 'button'

    button_id = Column(BigInteger, primary_key=True)
    MD_session_rawdata_row_id = Column(BigInteger, ForeignKey('MD_session_rawdata.row_id'), nullable=False)
    button_name = Column(String)

class State(Base):
    __tablename__ = 'state'

    state_id = Column(Integer, primary_key=True)
    button_name = Column(String)
    MD_session_rawdata_row_id = Column(BigInteger, ForeignKey('MD_session_rawdata.row_id'), nullable=False)

class MD_session_preprocessed(Base):
    __tablename__ = 'MD_session_preprocessed'

    row_id = Column(Integer, primary_key=True)
    
    MD_session_rawdata_row_id = Column(BigInteger, ForeignKey('MD_session_rawdata.row_id'), nullable=False)
    time = Column(Float)
    x = Column(Integer)
    y = Column(Integer)
    MD_session_featuresrow_id = Column(Integer, ForeignKey('MD_session_features.row_id'))

    md_session_features = relationship('MD_session_features', backref='md_session_preprocessed')

class MD_session_features(Base):
    __tablename__ = 'MD_session_features'

    row_id = Column(BigInteger, primary_key=True)
    MD_session_rawdata_row_id = Column(BigInteger, ForeignKey('MD_session_rawdata.row_id'))

    s_session_id = Column(Integer)
    user_id = Column(Integer, ForeignKey('User.user_id'), nullable=False)
    start_time = Column(Float)
    end_time = Column(Float)
    raw_ids = Column(JSON)

    direction_bin = Column(Integer)
    actual_distance = Column(Float)
    actual_distance_bin = Column(Integer)
    curve_length = Column(Float)
    curve_length_bin = Column(Integer)
    length_ratio = Column(Float)
    actual_speed = Column(Float)
    curve_speed = Column(Float)
    curve_acceleration = Column(Float)
    mean_movement_offset = Column(Float)
    mean_movement_error = Column(Float)
    mean_movement_variability = Column(Float)
    mean_curvature = Column(Float)
    mean_curvature_change_rate = Column(Float)
    mean_curvature_velocity = Column(Float)
    mean_curvature_velocity_change_rate = Column(Float)
    mean_angular_velocity = Column(Float)
    min_x = Column(Integer)
    max_x = Column(Integer)
    mean_x = Column(Float)
    std_x = Column(Float)
    max_min_x = Column(Integer)
    min_y = Column(Integer)
    max_y = Column(Integer)
    mean_y = Column(Float)
    std_y = Column(Float)
    max_min_y = Column(Integer)
    min_vx = Column(Float)
    max_vx = Column(Float)
    mean_vx = Column(Float)
    std_vx = Column(Float)
    max_min_vx = Column(Float)
    min_vy = Column(Float)
    max_vy = Column(Float)
    mean_vy = Column(Float)
    std_vy = Column(Float)
    max_min_vy = Column(Float)
    min_v = Column(Float)
    max_v = Column(Float)
    mean_v = Column(Float)
    std_v = Column(Float)
    max_min_v = Column(Float)
    min_a = Column(Float)
    max_a = Column(Float)
    mean_a = Column(Float)
    std_a = Column(Float)
    max_min_a = Column(Float)
    min_j = Column(Float)
    max_j = Column(Float)
    mean_j = Column(Float)
    std_j = Column(Float)
    max_min_j = Column(Float)
    min_am = Column(Float)
    max_am = Column(Float)
    mean_am = Column(Float)
    std_am = Column(Float)
    max_min_am = Column(Float)
    min_av = Column(Float)
    max_av = Column(Float)
    mean_av = Column(Float)
    std_av = Column(Float)
    max_min_av = Column(Float)
    min_c = Column(Float)
    max_c = Column(Float)
    mean_c = Column(Float)
    std_c = Column(Float)
    max_min_c = Column(Float)
    min_ccr = Column(Float)
    max_ccr = Column(Float)
    mean_ccr = Column(Float)
    std_ccr = Column(Float)
    max_min_ccr = Column(Float)
    duration_of_movement = Column(Float)
    straightness = Column(Float)
    TCM = Column(Float)
    SC = Column(Float)
    M3 = Column(Float)
    M4 = Column(Float)
    TCrv = Column(Float)
    VCrv = Column(Float)

    # user = relationship('User', back_populates='md_session_rawdata')
    # session = relationship('Session', back_populates='md_session_features')


#=============================================================================#
#====================MS=======================================================#
#=============================================================================#
class MS_session_rawdata(Base):
    __tablename__ = 'MS_session_rawdata'

    row_id = Column(Integer, primary_key=True)
    User_user_id = Column(Integer, ForeignKey('User.user_id'), nullable=False)
    Session_id = Column(Integer, ForeignKey('Session.id'), nullable=False)
    time = Column(TIMESTAMP)
    username = Column(String)
    winsession_id = Column(LargeBinary)
    intsession_id = Column(Integer)
    hwnd_orig = Column(LargeBinary)
    processname = Column(String)
    PID = Column(Integer)
    hookmode = Column(Integer)
    provocation = Column(Integer)
    message_id = Column(Integer)
    x_pos = Column(Integer)
    y_pos = Column(Integer)
    hwnd = Column(LargeBinary)
    hittest = Column(BigInteger)
    extra_info = Column(BigInteger)

    user = relationship('User', back_populates='ms_session_rawdata')
    session = relationship('Session', back_populates='ms_session_rawdata')

class MS_session_preprocessed(Base):
    __tablename__ = 'MS_session_preprocessed'

    row_id = Column(Integer, primary_key=True)
    MS_session_rawdata_row_id = Column(Integer, ForeignKey('MS_session_rawdata.row_id'), nullable=False)
    time = Column(TIMESTAMP)
    x = Column(Integer)
    y = Column(Integer)

    ms_session_rawdata = relationship('MS_session_rawdata', backref='ms_session_preprocessed')

#=============================================================================#
#====================KS=======================================================#
#=============================================================================#
class KD_session_rawdata(Base):
    __tablename__ = 'KD_session_rawdata'

    row_id = Column(Integer, primary_key=True)
    User_user_id = Column(Integer, ForeignKey('User.user_id'), nullable=False)
    Session_id = Column(Integer, ForeignKey('Session.id'), nullable=False)
    time = Column(TIMESTAMP)
    username = Column(String)
    winsession_id = Column(LargeBinary)
    intsession_id = Column(Integer)
    hwnd_orig = Column(LargeBinary)
    processname = Column(String)
    PID = Column(Integer)
    hookmode = Column(Integer)
    provocation = Column(Integer)
    virtualcode = Column(Integer)
    scancode = Column(Integer)
    keyup = Column(Boolean)
    prev_keystate = Column(Boolean)
    repeat_count = Column(Integer)
    alt_down = Column(Boolean)
    extended_kbd = Column(Integer)
    langID = Column(LargeBinary)
    keybdID = Column(LargeBinary)
    raw_data_debug = Column(LargeBinary)

    user = relationship('User', back_populates='kd_session_rawdata')
    session = relationship('Session', back_populates='kd_session_rawdata')

class KD_session_preprocessed(Base):
    __tablename__ = 'KD_session_preprocessed'

    row_id = Column(Integer, primary_key=True)
    KD_session_rawdata_row_id = Column(Integer, ForeignKey('KD_session_rawdata.row_id'), nullable=False)
    time_diff = Column(TIMESTAMP)
    di_virtualcode = Column(Integer)
    time_diff_up_up = Column(TIMESTAMP)
    time_diff_down_up = Column(TIMESTAMP)
    time_down = Column(TIMESTAMP)
    key_class = Column(Integer)

    kd_session_rawdata = relationship('KD_session_rawdata', backref='kd_session_preprocessed')

class KD_session_features(Base):
    __tablename__ = 'KD_session_features'

    id = Column(Integer, primary_key=True)
    KD_session_rawdata_row_id = Column(Integer, ForeignKey('KD_session_rawdata.row_id'), nullable=False)
    KD_session_preprocessed_row_id = Column(Integer, ForeignKey('KD_session_preprocessed.row_id'))

    kd_session_rawdata = relationship('KD_session_rawdata', backref='kd_session_features')
    kd_session_preprocessed = relationship('KD_session_preprocessed', backref='kd_session_features')

#=============================================================================#
#====================KS=======================================================#
#=============================================================================#
class KS_session_rawdata(Base):
    __tablename__ = 'KS_session_rawdata'

    row_id = Column(Integer, primary_key=True)
    User_user_id = Column(Integer, ForeignKey('User.user_id'), nullable=False)
    Session_id = Column(Integer, ForeignKey('Session.id'), nullable=False)
    time = Column(TIMESTAMP)
    username = Column(String)
    winsession_id = Column(LargeBinary)
    intersession_id = Column(Integer)
    hwnd_orig = Column(LargeBinary)
    processname = Column(String)
    PID = Column(Integer)
    hookmode = Column(Integer)
    provocation = Column(Integer)
    virtualcode = Column(Integer)
    scancode = Column(Integer)
    keyup = Column(Boolean)
    prev_keystate = Column(Boolean)
    repeat_count = Column(Integer)
    alt_down = Column(Boolean)
    extended_kbd = Column(Integer)
    langID = Column(LargeBinary)
    keybdID = Column(LargeBinary)
    raw_data_debug = Column(LargeBinary)

    user = relationship('User', back_populates='ks_session_rawdata')
    session = relationship('Session', back_populates='ks_session_rawdata')

class KS_session_preprocessed(Base):
    __tablename__ = 'KS_session_preprocessed'

    row_id = Column(Integer, primary_key=True)
    KS_session_rawdata_row_id = Column(Integer, ForeignKey('KS_session_rawdata.row_id'), nullable=False)
    down_time = Column(Float)
    dd_time = Column(Float)
    ud_time = Column(Float)

    ks_session_rawdata = relationship('KS_session_rawdata', backref='ks_session_preprocessed')



#=============================================================================#
#======================main===================================================#
#=============================================================================#
if __name__ == "__main__":
    username = "postgres"
    password = "Qwer17345"
    hostname = "localhost"
    port = "5000"
    database_name = "postgres"
    engine = create_engine(f'postgresql://{username}:{password}@{hostname}:{port}/{database_name}')
    Base.metadata.create_all(engine)
