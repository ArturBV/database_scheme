from sqlalchemy import Column, Integer, BigInteger, Text, JSON, ForeignKey, create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker

Base = declarative_base()

class User(Base):
    __tablename__ = 'User'
    user_id = Column(Integer, primary_key=True)
    username = Column(Text, nullable=False)

class Session(Base):
    __tablename__ = 'Session'
    s_id = Column(BigInteger, primary_key=True)
    user_id = Column(Integer, ForeignKey('User.user_id'), nullable=False)
    params = Column(JSON, nullable=False)
    user = relationship('User', back_populates='sessions')

class SessionRawdata(Base):
    __tablename__ = 'SessionRawdata'
    row_id = Column(BigInteger, primary_key=True, autoincrement=True)
    type = Column(Integer, nullable=False)
    user_id = Column(Integer, ForeignKey('User.user_id'), nullable=False)
    s_id = Column(BigInteger, ForeignKey('Session.s_id'), nullable=False)
    data = Column(JSON, nullable=False)
    user = relationship('User', back_populates='session_rawdata')
    session = relationship('Session', back_populates='session_rawdata')

class SessionFeatures(Base):
    __tablename__ = 'SessionFeatures'
    row_id = Column(BigInteger, primary_key=True, autoincrement=True)
    type = Column(Integer, nullable=False)
    user_id = Column(Integer, ForeignKey('User.user_id'), nullable=False)
    s_id = Column(BigInteger, ForeignKey('Session.s_id'), nullable=False)
    data = Column(JSON, nullable=False)
    user = relationship('User', back_populates='session_features')
    session = relationship('Session', back_populates='session_features')

ALL_TABLES_CLS = Base.__subclasses__()

User.sessions = relationship('Session', order_by=Session.s_id, back_populates='user')
User.session_rawdata = relationship('SessionRawdata', order_by=SessionRawdata.row_id, back_populates='user')
User.session_features = relationship('SessionFeatures', order_by=SessionFeatures.row_id, back_populates='user')
Session.session_rawdata = relationship('SessionRawdata', order_by=SessionRawdata.row_id, back_populates='session')
Session.session_features = relationship('SessionFeatures', order_by=SessionFeatures.row_id, back_populates='session')

