# MouseDyn
## Raw data
### Table1.1:
*from BALABIT dataset*

**TABLENAME: `raw_data_session_i`**

**example:**

|record timestamp|client timestamp|  button|state|  x|  y|
|----------------|----------------|--------|-----|---|---|
|             0.0|             0.0|NoButton| Move|263|506|
|  0.223999977112|  0.249000000302|NoButton| Move|263|502|
|  0.486000061035|  0.499000000302|NoButton| Move|263|500|
|  0.634000062943|  0.638999999966|NoButton| Move|264|499|
|  0.719000101089|  0.748000000138|NoButton| Move|266|479|

**domains:**

|       column_name|datatype         |Postgres Datatype|description(BALABIT descr)|
|       -----------|--------         |-----------------|-----------|
|`record_timestamp`| float64/float32 | REAL            |elapsed time (in sec) since the start of the session as recorded by the netork monitoring device|
|`client_timestamp`| float64/float32 | REAL            |elapsed time (in sec) since the start of the session as recorded by the RDP client|
|`button`          | string          | VARCHAR(50)     |the current condition of the mouse buttons|
|`state`           | string          | VARCHAR(50)     |additional information about the current state of the mouse|
|`x`               | int             |  INTEGER        |the x coordinate of the cursor on the screen|
|`y`               | int             |  INTEGER        |the y coordinate of the cursor on the screen|

-------------------

### or Table1.2:
*data already simply preprocessed*

**TABLENAME: `raw_data_session_i`**

**example:**

| id |      timestamp |    x |    y |  
| -- | -------------- | ---- | ---- |  
|  0 |            0.0 |  651 | 1352 |  
|  1 | 0.108999999997 |  653 | 1332 |  
|  2 | 0.217999999993 |  632 | 1329 | 

**domains:**

|       column_name|datatype         |Postgres Datatype|description|
|       -----------|--------         |-----------------|-----------|
|`id`              | long long       |  SERIAL         |row id     |
|`timestamp`       | float64(float32)|  REAL           |elapsed time (in sec) since the start of the session|
|`x`               | int             |  INTEGER        |the x coordinate of the cursor on the screen|
|`y`               | int             |  INTEGER        |the y coordinate of the cursor on the screen|


## Features

### Table2:

**domains(80):**

|       column_name                   |datatype   |Postgres Datatype |description [^1] [^2]|
|       -----------                   |--------   |----------------- |-----------|
| direction_bin                       |  float64  | DOUBLE PRECISION |  |
| actual_distance                     |  float64  | DOUBLE PRECISION |  |
| actual_distance_bin                 |  float64  | DOUBLE PRECISION |  |
| curve_length                        |  float64  | DOUBLE PRECISION |  |
| curve_length_bin                    |  float64  | DOUBLE PRECISION |  |
| length_ratio                        |  float64  | DOUBLE PRECISION |  |
| actual_speed                        |  float64  | DOUBLE PRECISION |  |
| curve_speed                         |  float64  | DOUBLE PRECISION |  |
| curve_acceleration                  |  float64  | DOUBLE PRECISION |  |
| mean_movement_offset                |  float64  | DOUBLE PRECISION |  |
| mean_movement_error                 |  float64  | DOUBLE PRECISION |  |
| mean_movement_variability           |  float64  | DOUBLE PRECISION |  |
| mean_curvature                      |  float64  | DOUBLE PRECISION |  |
| mean_curvature_change_rate          |  float64  | DOUBLE PRECISION |  |
| mean_curvature_velocity             |  float64  | DOUBLE PRECISION |  |
| mean_curvature_velocity_change_rate |  float64  | DOUBLE PRECISION |  |
| mean_angular_velocity               |  float64  | DOUBLE PRECISION |  |
| min_x                               |  float64  | DOUBLE PRECISION |  |
| max_x                               |  float64  | DOUBLE PRECISION |  |
| mean_x                              |  float64  | DOUBLE PRECISION |  |
| std_x                               |  float64  | DOUBLE PRECISION |  |
| max_min_x                           |  float64  | DOUBLE PRECISION |  |
| min_y                               |  float64  | DOUBLE PRECISION |  |
| max_y                               |  float64  | DOUBLE PRECISION |  |
| mean_y                              |  float64  | DOUBLE PRECISION |  |
| std_y                               |  float64  | DOUBLE PRECISION |  |
| max_min_y                           |  float64  | DOUBLE PRECISION |  |
| min_vx                              |  float64  | DOUBLE PRECISION |  |
| max_vx                              |  float64  | DOUBLE PRECISION |  |
| mean_vx                             |  float64  | DOUBLE PRECISION |  |
| std_vx                              |  float64  | DOUBLE PRECISION |  |
| max_min_vx                          |  float64  | DOUBLE PRECISION |  |
| min_vy                              |  float64  | DOUBLE PRECISION |  |
| max_vy                              |  float64  | DOUBLE PRECISION |  |
| mean_vy                             |  float64  | DOUBLE PRECISION |  |
| std_vy                              |  float64  | DOUBLE PRECISION |  |
| max_min_vy                          |  float64  | DOUBLE PRECISION |  |
| min_v                               |  float64  | DOUBLE PRECISION |  |
| max_v                               |  float64  | DOUBLE PRECISION |  |
| mean_v                              |  float64  | DOUBLE PRECISION |  |
| std_v                               |  float64  | DOUBLE PRECISION |  |
| max_min_v                           |  float64  | DOUBLE PRECISION |  |
| min_a                               |  float64  | DOUBLE PRECISION |  |
| max_a                               |  float64  | DOUBLE PRECISION |  |
| mean_a                              |  float64  | DOUBLE PRECISION |  |
| std_a                               |  float64  | DOUBLE PRECISION |  |
| max_min_a                           |  float64  | DOUBLE PRECISION |  |
| min_j                               |  float64  | DOUBLE PRECISION |  |
| max_j                               |  float64  | DOUBLE PRECISION |  |
| mean_j                              |  float64  | DOUBLE PRECISION |  |
| std_j                               |  float64  | DOUBLE PRECISION |  |
| max_min_j                           |  float64  | DOUBLE PRECISION |  |
| min_am                              |  float64  | DOUBLE PRECISION |  |
| max_am                              |  float64  | DOUBLE PRECISION |  |
| mean_am                             |  float64  | DOUBLE PRECISION |  |
| std_am                              |  float64  | DOUBLE PRECISION |  |
| max_min_am                          |  float64  | DOUBLE PRECISION |  |
| min_av                              |  float64  | DOUBLE PRECISION |  |
| max_av                              |  float64  | DOUBLE PRECISION |  |
| mean_av                             |  float64  | DOUBLE PRECISION |  |
| std_av                              |  float64  | DOUBLE PRECISION |  |
| max_min_av                          |  float64  | DOUBLE PRECISION |  |
| min_c                               |  float64  | DOUBLE PRECISION |  |
| max_c                               |  float64  | DOUBLE PRECISION |  |
| mean_c                              |  float64  | DOUBLE PRECISION |  |
| std_c                               |  float64  | DOUBLE PRECISION |  |
| max_min_c                           |  float64  | DOUBLE PRECISION |  |
| min_ccr                             |  float64  | DOUBLE PRECISION |  |
| max_ccr                             |  float64  | DOUBLE PRECISION |  |
| mean_ccr                            |  float64  | DOUBLE PRECISION |  |
| std_ccr                             |  float64  | DOUBLE PRECISION |  |
| max_min_ccr                         |  float64  | DOUBLE PRECISION |  |
| duration_of_movement                |  float64  | DOUBLE PRECISION |  |
| straightness                        |  float64  | DOUBLE PRECISION |  |
| TCM                                 |  float64  | DOUBLE PRECISION |  |
| SC                                  |  float64  | DOUBLE PRECISION |  |
| M3                                  |  float64  | DOUBLE PRECISION |  |
| M4                                  |  float64  | DOUBLE PRECISION |  |
| TCrv                                |  float64  | DOUBLE PRECISION |  |
| VCrv                                |  float64  | DOUBLE PRECISION |  |

[^1]: ![features_1](features_1.png "features_explanation_1")
[^2]: ![features_2](features_2.png "features_explanation_2")